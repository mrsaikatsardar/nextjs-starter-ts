import Welcome from "../components/Welcome";

const index = () => {
	return (
		<>
			<div className='text-center text-2xl'>
				<Welcome />
			</div>
		</>
	);
};

export default index;
